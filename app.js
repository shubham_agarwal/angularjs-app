angular.module('chat', ['firebase'])
  .controller('Chat', ['$scope', '$timeout', 'angularFireCollection',
    function($scope, $timeout, angularFireCollection) {
    	var url = 'https://text-content123.firebaseio.com/content';		//storing database at this url
	$scope.text = angularFireCollection(new Firebase(url));
	$scope.colors = ['red' , 'green' , 'blue'];
	$scope.index = 0;
	$scope.wordSelection = function(){
		 var sel;

    // Check for existence of window.getSelection() and that it has a
    // modify() method. IE 9 has both selection APIs but no modify() method.
    if (window.getSelection && (sel = window.getSelection()).modify) {
        sel = window.getSelection();
        if (!sel.isCollapsed) {

            // Detect if selection is backwards
            var range = document.createRange();
            range.setStart(sel.anchorNode, sel.anchorOffset);
            range.setEnd(sel.focusNode, sel.focusOffset);
            var backwards = range.collapsed;
            range.detach();

            // modify() works on the focus of the selection
            var endNode = sel.focusNode, endOffset = sel.focusOffset;
            sel.collapse(sel.anchorNode, sel.anchorOffset);

            var direction = [];
            if (backwards) {
                direction = ['backward', 'forward'];
            } else {
                direction = ['forward', 'backward'];
            }

            sel.modify("move", direction[0], "character");
            sel.modify("move", direction[1], "word");
            sel.extend(endNode, endOffset);
            sel.modify("extend", direction[1], "character");
            sel.modify("extend", direction[0], "word");
		range = sel.getRangeAt(0);
		var rangeTxt = range.toString();
		return range;
      			  }
		}
	}	
	
	//higlighting the selcted text
		$scope.highlight = function(){
		var editableDiv = document.getElementById("textArea");
		selText = window.getSelection();
		if (selText.rangeCount == 0) 
		{	
			console.log("no selection");
			return;
		}
   		var range = $scope.wordSelection();
		if(!range)
		{
			return;		
		}

		var selectionColor = $scope.colors[$scope.index % 3];
		var span = $('<span class="selected">').css('color', selectionColor)[0];
		span.appendChild(range.extractContents());
    		range.insertNode(span);
		$scope.index++;
		var html = editableDiv.innerHTML;
		$scope.text[0].para = html;	
		$scope.text.update('content1');		
		}
		
    }

  ]);
	



